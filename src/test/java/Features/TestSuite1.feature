@Test1
Feature: This feature tests various types of webElements with dynamic locators

  Scenario: This test Clicks on blue button and get its class attribute
    Given link "https://the-internet.herokuapp.com/challenging_dom"
    When user clicks on "blue button"
    Then expected attribute for class is "button"

  Scenario: This test Clicks on red button and get its class attribute
    Given link "https://the-internet.herokuapp.com/challenging_dom"
    When user clicks on "red button"
    Then expected attribute for class is "button alert"

  Scenario: This test Clicks on green button and get its class attribute
    Given link "https://the-internet.herokuapp.com/challenging_dom"
    When user clicks on "green button"
    Then expected attribute for class is "button success"

  Scenario Outline: This test getText of any specified element of table provided row and column id are mentioned
    Given link "https://the-internet.herokuapp.com/challenging_dom"
    When user provides row_id "<row_id>" and col_id "<col_id>"
    Then print the value of text at that position

    Examples:

      | row_id | col_id |
      | 1      | 2      |
      | 5      | 4      |
      | 3      | 1      |
      | 10     | 6      |

  Scenario: Get the headers of the table
    Given link "https://the-internet.herokuapp.com/challenging_dom"
    When table element is found
    Then print the headers of the table

  Scenario: Get the number of table columns
    Given link "https://the-internet.herokuapp.com/challenging_dom"
    When table column elements is found
    Then print the number of table columns

  Scenario: Get the number of table rows
    Given link "https://the-internet.herokuapp.com/challenging_dom"
    When table row element is found
    Then print the number of table rows

  Scenario: Get the current url when clicked on edit of 1st row
    Given link "https://the-internet.herokuapp.com/challenging_dom"
    When click on row "2" edit hyperlink
    Then print the currenturl

  Scenario: Get the current url when clicked on delete of 7th row
    Given link "https://the-internet.herokuapp.com/challenging_dom"
    When click on row "7" delete hyperlink
    Then print the currenturl

  Scenario: Take screenshot of the canvas element and store it in taget folder
    Given link "https://the-internet.herokuapp.com/challenging_dom"
    When focus on canvas element
    Then TakeScreenshot and place it with name "Canvas" in target folder "./target/Screenshot/"
