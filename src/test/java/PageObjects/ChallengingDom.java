package PageObjects;


import org.openqa.selenium.By;

public class ChallengingDom {

    public static By blue_button = By.xpath("//a[@class=\"button\"]");
    public static By red_button = By.xpath("//a[@class=\"button alert\"]");
    public static By green_button = By.xpath("//a[@class=\"button success\"]");
    public static By table_headers = By.xpath("//table/thead/tr/th");
    public static By table_rows = By.xpath("//table/tbody/tr");
    public static By canvas = By.xpath("//canvas[@id = 'canvas']");
}
