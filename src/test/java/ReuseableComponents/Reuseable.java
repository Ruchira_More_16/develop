package ReuseableComponents;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Reuseable {

    public static void click(WebDriver driver, By element) {
        driver.findElement(element);
    }

    public static String getElementFromTableonChallengingDom(WebDriver driver, int row_id, int col_id) {
        String element = null;
        for (int i = 1; i <= row_id; i++) {
            for (int j = 1; j < col_id; j++) {

                WebElement ele = driver.findElement(By.xpath("//table/tbody/tr[" + row_id + "]/td[" + col_id + "]"));
                element = ele.getText();
            }
        }
        return element;
    }

    public static String getElementFromTableonChallengingDom(WebDriver driver, int row_id, String value) {
        String element = null;
        for (int i = 1; i <= row_id; i++) {
            if (value.equalsIgnoreCase("edit")) {
                WebElement ele = driver.findElement(By.xpath("//table/tbody/tr[" + row_id + "]/td[7]/a[1]"));
                ele.click();
                element = driver.getCurrentUrl();
            } else {
                WebElement ele = driver.findElement(By.xpath("//table/tbody/tr[" + row_id + "]/td[7]/a[2]"));
                ele.click();
                element = driver.getCurrentUrl();
            }
        }
        return element;
    }

    public static void TakeScreenshot(WebDriver driver, String path, String file_name) {

        File folder = new File(path);
        if (!folder.exists()) {
            folder.mkdir();
        }
        File dest = new File(path + file_name + ".jpg");
        TakesScreenshot screenshot = ((TakesScreenshot) driver);
        File src = screenshot.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(src, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void FocusOnElement(WebDriver driver, By element) {
        WebElement ele = driver.findElement(element);
        new Actions(driver).moveToElement(ele).perform();
    }

    public static void movetoLocation(WebDriver driver, By element) {
        WebElement ele = driver.findElement(element);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("window.scrollBy("+ ele.getLocation().getX()+","+ele.getLocation().getY()+");");
    }
}
