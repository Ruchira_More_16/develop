package TestScripts;

import PageObjects.ChallengingDom;
import ReuseableComponents.Reuseable;
import cucumber.api.java.en.*;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ReuseableComponents.Reuseable.*;

public class TestSet1 {
    static WebDriver driver = null;
    static String attr_button = null;
    static String value = null;
    static String str_url = null;


    public static void main(String args[]) throws InterruptedException {
        driver = IntitiateDriver();
        driver.get("https://the-internet.herokuapp.com/challenging_dom");
        Thread.sleep(1000);
        System.out.println(getElementFromTableonChallengingDom(driver, 6, 3));
    }

    public static WebDriver IntitiateDriver() {
        System.setProperty("webdriver.chrome.driver", ".//src//main//resources//Drivers//chromedriver.exe");
        driver = new ChromeDriver();
        return driver;
    }

    public static void driverclose() {
        driver.close();
    }

    @Given("^link \"(.*)\"$")
    public static void launchUrl(String endpoint) {
        driver = IntitiateDriver();
        driver.get(endpoint);
        driver.manage().window().maximize();
    }

    @When("^user clicks on \"(.*)\"$")
    public static void clickonbutton(String button) throws InterruptedException {
        Thread.sleep(3000);
        if (button.contains("blue")) {
            attr_button = driver.findElement(ChallengingDom.blue_button).getAttribute("class");
            driver.findElement(ChallengingDom.blue_button).click();
        } else if (button.contains("red")) {
            attr_button = driver.findElement(ChallengingDom.red_button).getAttribute("class");
            driver.findElement(ChallengingDom.red_button).click();
        } else {
            attr_button = driver.findElement(ChallengingDom.green_button).getAttribute("class");
            driver.findElement(ChallengingDom.green_button).click();
        }
    }

    @Then("^expected attribute for class is \"(.*)\"$")
    public static void matchValueString(String classname) throws InterruptedException {
        Thread.sleep(1000);
        if (classname.contains("alert")) {
            System.out.println(driver.findElement(ChallengingDom.red_button).getAttribute("class"));
            Assert.assertEquals(driver.findElement(ChallengingDom.red_button).getAttribute("class"), attr_button);
            driverclose();
        } else if (classname.contains("success")) {
            System.out.println(driver.findElement(ChallengingDom.green_button).getAttribute("class"));
            Assert.assertEquals(driver.findElement(ChallengingDom.green_button).getAttribute("class"), attr_button);
            driverclose();
        } else {
            System.out.println(driver.findElement(ChallengingDom.blue_button).getAttribute("class"));
            Assert.assertEquals(driver.findElement(ChallengingDom.blue_button).getAttribute("class"), attr_button);
            driverclose();
        }
    }

    @When("^user provides row_id \"(.*)\" and col_id \"(.*)\"$")
    public static void getelementoftable(String row_id, String col_id) {
        value = getElementFromTableonChallengingDom(driver, Integer.parseInt(row_id), Integer.parseInt(col_id));
    }

    @Then("^print the value of text at that position$")
    public static void printvalue() {
        System.out.println(value);
        driverclose();
    }

    @When("^table element is found$")
    public static void thederelement() {
        Assert.assertEquals(driver.findElement(ChallengingDom.table_headers).isDisplayed(), true);
    }

    @Then("^print the headers of the table$")
    public static void getheaderelements() {
        List<WebElement> ele = driver.findElements(ChallengingDom.table_headers);
        for (int i = 0; i < ele.size(); i++) {
            System.out.println("At position " + (i + 1) + ", the value is: " + ele.get(i).getText());
        }
        driverclose();
    }

    @When("^table column elements is found$")
    public static void col_count_ele() {
        Assert.assertEquals(driver.findElement(ChallengingDom.table_headers).isDisplayed(), true);
    }

    @Then("^print the number of table columns$")
    public static void col_count() {
        List<WebElement> ele = driver.findElements(ChallengingDom.table_headers);
        System.out.println("Number of rows are : " + ele.size());
        driverclose();
    }

    @When("^table row element is found$")
    public static void row_count_ele() {
        Assert.assertEquals(driver.findElement(ChallengingDom.table_rows).isDisplayed(), true);
    }

    @Then("^print the number of table rows$")
    public static void row_count() {
        List<WebElement> ele = driver.findElements(ChallengingDom.table_rows);
        System.out.println("Number of rows are : " + ele.size());
        driverclose();
    }

    @When("^click on row \"(.*)\" edit hyperlink$")
    public static void clickonedit(String count_id) {
        str_url = getElementFromTableonChallengingDom(driver, Integer.parseInt(count_id), "edit");
    }

    @When("^click on row \"(.*)\" delete hyperlink$")
    public static void clickondelete(String count_id) {
        str_url = getElementFromTableonChallengingDom(driver, Integer.parseInt(count_id), "delete");
    }

    @Then("^print the currenturl$")
    public static void printcurrUrl() {
        System.out.println(str_url);
        driverclose();
    }

    @When("^focus on canvas element$")
    public static void focusOnCanvaselement() {
        movetoLocation(driver, ChallengingDom.canvas);
    }

    @Then("^TakeScreenshot and place it with name \"(.*)\" in target folder \"(.*)\"$")
    public static void capturescreen(String filename, String path) {
        TakeScreenshot(driver, path, filename);
        driverclose();
    }

}
